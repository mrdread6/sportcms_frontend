import MatchCarouselEntry from "./MatchCarouselEntry.vue"
import MatchCarousel from "./MatchesCarousel.vue"

export {
    MatchCarouselEntry,
    MatchCarousel,
}