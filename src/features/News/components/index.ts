import NewsPreviewBig from "./NewsPreviewBig.vue"
import NewsPreviewSmall from "./NewsPreviewSmall.vue"
import Card from "./Card.vue"
import CardsGrid from "./CardsGrid.vue"

export {
    NewsPreviewBig,
    NewsPreviewSmall,
    CardsGrid,
    Card,
}