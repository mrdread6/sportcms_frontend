import { Article } from "@/types";
import axios, { AxiosResponse, HttpStatusCode } from "axios";

class ArticleService{
    getLatestArticles(pageNumber : number, size : number) : Promise<AxiosResponse<Article[]>>{
        return axios.get<Article[]>('/api/v1/public/articles',{
            params: {
                page: pageNumber,
                pageSize : size,
            }
        });
    }
    getArticle(articleID : Number) : Promise<AxiosResponse<Article>>{
        return axios.get<Article>('/api/v1/public/article',{
            params:{
                articleID : articleID
            }
        });
    }
    createArticle(form : FormData, auth : String){
        axios.post('/api/v1/article/create',{
            data: form,
            headers: { 
                "Content-Type": "multipart/form-data",
                'Authorization': 'Bearer ' + auth,
            },
        });
    }
}
export default new ArticleService()