import { NewsPreviewBig, NewsPreviewSmall } from "./components";

export {
    NewsPreviewBig,
    NewsPreviewSmall,
}