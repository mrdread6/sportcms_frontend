import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/NewsFeed',
    name: 'NewsFeed',
    component: () => import('@/features/News/views/NewsFeed.vue')
  },
  {
    path: '/article/:articleId',
    name: 'Article',
    props: true,
    component: () => import('@/features/News/views/ArticleView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
