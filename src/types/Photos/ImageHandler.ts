class ImageHandler{
    private blob : Blob;
    private url : string;
    id: any;

    public setBlob(image : Blob){
        this.blob = image;
        this.url = URL.createObjectURL(image)
    }
    public getBlob() : Blob{
        return this.blob;
    }
    public getUrl() : String{
        return this.url;
    }
    constructor(){
        this.blob = new Blob();
        this.url = '';
    }
}
export default ImageHandler;