import ArticleState from "./ArticleState";
import Photo from "../Photos/Photo";

interface NewArticle{
    content : string,
    creatorId : number,
    publishDate : string,
    status : ArticleState,
    photos: Photo[],
}

export default NewArticle;