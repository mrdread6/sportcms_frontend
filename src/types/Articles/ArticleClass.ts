import Article from "./Article";
import ArticleState from "./ArticleState";
import Photo from "../Photos/Photo";

class ArticleClass implements Article {
    id: number;
    content: string;
    creatorId: number;
    publishDate: string;
    status: ArticleState;
    photos: Photo[];

    constructor(id : number, content : string, creatorId : number, publishDate : string, status : ArticleState, photos : Photo[]){
        this.id = id;
        this.content = content;
        this.creatorId = creatorId;
        this.publishDate = publishDate;
        this.status = status;
        this.photos = photos;
    }

    updateState(newState : ArticleState){
        this.status = newState;
    }
    public getAbstract() {
        const regex = /<p>(.*?)<\/p>/;
        const firstPMatch = this.content.match(regex);
        if (firstPMatch)
            return firstPMatch[0];
        else
            return ''
    }
    
    public getHeaderText() {
        const regex = /<h1>(.*?)<\/h1>/;
        const firstH1Match = this.content.match(regex);
        if (firstH1Match)
            return firstH1Match[0];
        else
            return ''
    }
}
export default ArticleClass;