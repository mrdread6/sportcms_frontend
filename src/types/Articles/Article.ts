import NewArticle from "./NewArticle";

interface Article extends NewArticle{
    id: number
}
export default Article;