interface Match{
    id: Number,
    isLive: Boolean,
    date: Date,
    teamA: Number,
    teamB: Number,
    scores: [],
}