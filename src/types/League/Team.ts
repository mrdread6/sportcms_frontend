import { ImageHandler } from "@/features/News/services";

interface Team{
    id: Number,
    short: String,
    name: String,
    logo: ImageHandler,
}