import axios, { AxiosResponse } from "axios";

class ImageService {
  getImage(id: number) : Promise<AxiosResponse<Blob>>{
    return axios.get(`/api/v1/public/photo/show/${id}`,{
        responseType: 'blob'
    });
  }
}
export default new ImageService();
