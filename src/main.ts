import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from '@/store/index'

import '@/registerServiceWorker'
import '@/styles/main.scss'

const app = createApp(App)

app.use(router)

router.beforeEach(function (to, from, next) { 
    setTimeout(() => {
        window.scrollTo(0, 0);
    }, 100);
    next();
});

app.use(store)
app.mount('#app')
