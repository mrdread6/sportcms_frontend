import ImageService from "@/api/ImageService";
import { ImageHandler } from "@/types";

interface State {
  images: Array<{ ImageId: number; Image: ImageHandler }>;
}

const state: State = {
  images: [],
};

const mutations = {
  storePhoto(
    state: State,
    { ImageId, Image }: { ImageId: number; Image: ImageHandler }
  ) {
    state.images.push({ ImageId, Image });
  },
};

const actions = {
  storePhoto({ commit }: any, { Image }: { Image: ImageHandler }) {
    commit("storePhoto", { ImageId: Image.id, Image: Image });
  },
  pullImage({ commit }: any, { id }: { id: number }) {
    if (id == undefined) return;
    if (state.images.find((image) => image.ImageId == id)) {
      return;
    }
    ImageService.getImage(id)
      .then((response: { data: Blob }) => response.data)
      .then((data: Blob) => {
        const handler = new ImageHandler();
        handler.setBlob(data);
        commit("storePhoto", { ImageId: id, Image: handler });
      });
  },
};

const getters = {
  getImage: (state: State) => (id: number) => {
    return state.images.find((image) => image.ImageId == id);
  },
};

const ArticleStore = {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
export default ArticleStore;
