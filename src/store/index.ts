import { createStore } from "vuex";
import ArticleStore from "./ArticleStore";
import ImageStore from "./ImageStore";

const store = createStore({
    modules: {
        ArticleStore,
        ImageStore,
    },
})

export default store;