import ArticleService from "@/features/News/api/ArticleService";
import { Article, ArticleClass } from "@/types";

interface State {
  articles: Array<{ articleId: number; article: ArticleClass }>;
}

const state: State = {
  articles: [],
};

const mutations = {
  storeArticle(
    state: State,
    { articleId, article }: { articleId: number; article: ArticleClass }
  ) {
    state.articles.push({ articleId, article });
  },
};

const actions = {
  storeArticle({ commit }: any, { article }: { article: ArticleClass }) {
    commit("storeArticle", { articleId: article.id, article: article });
  },
  pullArticles(
    { commit }: any,
    { page, size }: { page: number; size: number }
  ) {
    ArticleService.getLatestArticles(page, size)
      .then((Response) => Response.data)
      .then((data) => {
        data.forEach((element: Article) => {
          const tmp = {
            articleId: element.id,
            article: new ArticleClass(
              element.id,
              element.content,
              element.creatorId,
              element.publishDate,
              element.status,
              element.photos
            ),
          };
          if (
            !state.articles.find(
              (article) => article.articleId == tmp.articleId
            )
          )
            commit("storeArticle", tmp);
        });
      });
  },
  pullArticle({ commit }: any, { id }: { id: number }) {
    if (state.articles.find((article) => article.articleId == id)) return;
    ArticleService.getArticle(id)
      .then((Response) => Response.data)
      .then((data) => {
        commit("storeArticle", { articleId: data.id, article: data });
      });
  },
};

const getters = {
  articlesByPage: (state: State) => (page: number, size: number) => {
    return state.articles.slice(0, page * size + size);
  },
  article: (state: State) => (id: number) => {
    return state.articles.find((article) => article.articleId == id);
  },
  articles: (state: State) => () => {
    return state.articles;
  },
};

const ArticleStore = {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
export default ArticleStore;
